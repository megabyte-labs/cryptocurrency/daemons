FROM ubuntu:latest as builder

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get -y --no-install-recommends install \
        ca-certificates \
        cmake \
        doxygen \
        git \
        graphviz \
        g++ \
        libboost1.58-all-dev \
        libreadline-dev \
        libsodium-dev \
        libssl-dev \
        libzmq3-dev \
        make \
        pkg-config \
    && git clone https://github.com/graft-project/GraftNetwork.git /root/graft \
    && cd /root/graft \
    && rm -rf build \
    && make release-static
    
FROM ubuntu:latest

WORKDIR /opt/graft

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y --no-install-recommends \
        ca-certificates \
        curl \
        gnupg \
        libboost1.58-all-dev \
        software-properties-common \
    && curl -sL https://deb.nodesource.com/setup_9.x | bash \
    && apt-get install -y nodejs \
    && npm install -g pm2 \
    && apt-get autoclean \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/*

COPY --from=builder /root/graft/build/release/bin/* /opt/graft/

EXPOSE 44433 44434

CMD ["pm2-docker", "process.json"]
